import puppeteer from 'puppeteer';
import fs from 'fs';
import * as dotenv from 'dotenv';
import axios from "axios";

// Process args passed from the outside
let args = {
  // These are the default values for when no args are passed
  showThumbnails: false,
  groupMessages: false,
  urls: './URLS_TO_CHECK.txt',
};

process.argv.slice(2).forEach((val) => {
  const arg = val.split('=');
  const key = arg[0].replace('--', '');
  args[key] = arg.length >= 2 ? arg[1] : true;
});

dotenv.config();

const log = (...args) => {
  const date = new Date().toLocaleDateString('nl-NL');
  const time = new Date().toLocaleTimeString('nl-NL', { hour: "numeric", minute: "numeric", second: "numeric"});
  console.log(`[${date} ${time}]`, ...args);
}

const wait = ms => new Promise(resolve => setTimeout(resolve, ms))

const { LAUNCH_MODE, BROWSER_PATH, BROWSER_URL, BROWSER_WS_ENDPOINT } = process.env;

let browser;
if (LAUNCH_MODE === 'dev') {
  // This mode is for Windows and uses the installed chrome instance
  browser = await puppeteer.launch();
} else if (LAUNCH_MODE === 'executablePath') {
  // This mode is for Ubuntu/Unix-like systems
  browser = await puppeteer.connect({
    executablePath: BROWSER_PATH || '/usr/bin/chromium-browser',
  });
} else if (LAUNCH_MODE === 'url') {
  // This mode is for connecting to a headless browser instance via URL (e.g. for chrome running in a docker container)
  browser = await puppeteer.connect({
    browserURL: BROWSER_URL || 'http://localhost:3000',
  });
} else {
  // (DEFAULT) This mode is for connecting to a headless browser instance via web sockets (e.g. for chrome running in a docker container)
  browser = await puppeteer.connect({
    browserWSEndpoint: BROWSER_WS_ENDPOINT || 'ws://localhost:3000'
  });
}

const CACHE_PATH = `./CACHE.txt`;
const URLS_PATH = args.urls;

const URLS = fs.readFileSync(URLS_PATH, { encoding:'utf8', flag:'r' }).split('\n').map(url => url.trim()).filter(url => !!url);

if (!URLS.length) {
  throw new Error(`No URL's were specified in "${URLS_PATH}"`);
}

const selectors = {
  list: '.hz-Listings.hz-Listings--list-view',
  listItem: `.hz-Listing.hz-Listing--list-item, .hz-Listing.hz-Listing--list-item-cars`,
  listItemLink: `.hz-Link.hz-Link--block.hz-Listing-coverLink`,
  listItemLinkTitle: `.hz-Listing-title`,
  listItemLinkPriority: `.hz-Listing-priority.hz-Listing-priority--all-devices`,
  listItemLinkDate: `.hz-Listing-date.hz-Listing-date--desktop`,
  listItemLinkLocation: `.hz-Listing-location`,
  itemCreatedOn: `.hz-Icon.hz-Icon--textSecondary.hz-SvgIcon.hz-SvgIconClock + span`,
  notFound: '.hz-Alert.hz-Alert--info.zrp-search-helper-container',
};

function readCache() {
  try {
    const cache = fs.readFileSync(CACHE_PATH,{ encoding:'utf8', flag:'r' });
    return cache?.split('\n') || [];
  } catch {
    return [];
  }
}

async function getListFromPage(url) {
  try {
    const page = await browser.newPage();

    await page.goto(url);

    // Try to find the search-results
    await page.waitForSelector(selectors.list, { timeout: 5000 }).catch(async () => {
      // If it can't find the list of results, try to find the "Helaas no results 🖕" message
      await page.waitForSelector(selectors.notFound, { timeout: 5000 }).catch(async () => {
        // If it also can't find that, throw the error
        await page.screenshot({ path: `./error.jpg` });
        throw new Error('No search-results found on page, and also the "no results" message is not shown. Please check error.jpg for more details');
      });
    });

    // The href on Marktplaats links will be null until you mouse-over it :(
    // This page.evaluate will MouseOver all the links so that the href's will be created
    log('Hovering over links...');
    await page.evaluate(({ list, listItem, listItemLink }) => [
      ...document.querySelectorAll(`${list} > ${listItem}`)
    ].map((item) => {
      const anchor = item.querySelector(listItemLink);
      anchor.dispatchEvent(new MouseEvent('mouseover', {
        'view': window,
        'bubbles': true,
        'cancelable': true,
      }));
    }), selectors);

    log('Waiting for the links...');
    // Wait a bit for the href attributes to be added to the links by Marktplaats
    await wait(1000);

    // Perfect time to create a debug screenshot :)
    await page.screenshot({ path: `./debug.jpg` });

    log('Collecting links...');
    // Create a list of links
    const links = (await page.evaluate(({list, listItem, listItemLink, listItemLinkPriority}) => {
      return [...document.querySelectorAll(`${list} > ${listItem}`)].map((item) => {
        const anchor = item.querySelector(listItemLink);

        if (item.querySelector(listItemLinkPriority)?.textContent === 'Topadvertentie') {
          // we don't care about Topadvertenties
          return null;
        }
        return anchor.href;
      });
    }, selectors)).filter(link => !!link);
    log(`Found ${links.length} links on the page (excluding TopAdvertenties)`);

    return [...new Set(links)].filter(item => !!item);
  } catch (error) {
    log('Error:', error);
    return [];
  }
}

function getNewListItems(oldList, newList) {
  return newList.filter(item => !oldList.includes(item));
}

function writeCache(dataArray) {
  fs.writeFileSync(CACHE_PATH, dataArray.join('\n'));
}

async function getMetaDataFromItem(url) {
  const page = await browser.newPage();
  log('Getting metaData from url', url);
  await page.goto(url);
  return await page.evaluate(({ itemCreatedOn }) => {
    return {
      createdOn: document.querySelector(itemCreatedOn)?.textContent,
    }
  }, selectors);
}

async function notify(newUrls) {
  const { TELEGRAM_BOT_TOKEN, TELEGRAM_CHAT_ID } = process.env;

  async function send(text) {
    await axios.post(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage?disable_web_page_preview=${!args.showThumbnails}`, {
      chat_id: TELEGRAM_CHAT_ID,
      // A telegram message can not be longer than 4096 characters, we cut it off here if it's needed
      text: text.slice(0, 4096),
    }).then(() => log('Telegram message sent successfully!')).catch(error => {
      console.error(error);
      throw new Error(`Failed to send telegram message to chat with chatId "${TELEGRAM_CHAT_ID}"`);
    });
  }

  let groupedText = '';
  for (let [index, url] of newUrls.entries()) {
    const metaData = await getMetaDataFromItem(url);

    const text = `[${metaData?.createdOn?.replace('sinds ', '')}] ${url.replace('www.marktplaats.nl', 'link.marktplaats.nl')}`;

    if (args.groupMessages) {
      // We group multiple urls in a single telegram message as long as the amount of characters is below 4096
      // because there is a limit of sending a maximum of 20 messages per minute.
      if ((groupedText + text).length >= 4050) {
        await send(groupedText);
        groupedText = text;
      } else {
        groupedText += `\n${text}`;
      }

      // Send text if the last url has been reached
      if (index === newUrls.length - 1) {
        await send(groupedText);
      }
    } else {
      // When we don't want to group messages, we send them one by one.
      // Since a  bot will not be able to send more than 20 messages per minute to the same group,
      // we add a delay of 3 seconds in between each message
      await send(text);
      log('Waiting 3 seconds before sending the next message...');
      await wait(3050);
    }
  }
  return true;
}

log('Starting...');
let currentList = [];
for (let url of URLS) {
  log(`Loading url ${url}...`);
  currentList = [...currentList, ...(await getListFromPage(url))];
}
currentList = [...new Set(currentList)];

const cachedList = readCache();
const newListItems = getNewListItems(cachedList, currentList);

if (newListItems.length) {
  log('New items found! Attempting to send a telegram chat...');
  const success = await notify(newListItems);
  log(`Chat sent ${success ? 'Successfully' : 'Failed'}!`);
  if (success) {
    writeCache([...cachedList, ...newListItems]);
  } else {
    log('NOTE: the cache was not updated because the chat did not send.');
  }
} else {
  log('No new items found on page.');
}

log('disconnecting browser...');
await browser.close();
await browser.disconnect();
log('done!');

