# 🤷

1. make a `.env` file and provide some values
```sh
TELEGRAM_BOT_TOKEN="0000000000:AAAA-aaA000AAAaAA0AAaaaaaaAAAaaAAa"
TELEGRAM_CHAT_ID="-0000000000000"
LAUNCH_MODE="ws"
BROWSER_PATH="/usr/bin/chromium-browser"
BROWSER_URL="http://localhost:3000"
BROWSER_WS_ENDPOINT="ws://localhost:3000"
```

- `LAUNCH_MODE` can be set to `dev`, `path`, `url`, or `ws` (By default it will be `ws`)
- `BROWSER_PATH`, `BROWSER_URL` and `BROWSER_WS_ENDPOINT` can also be added to the `.env` but also have default values (the ones shown above)

2. Specify the Marktplaats URL's you want to check in `URLS_TO_CHECK.txt` (one URL per line)

3. Run `npm install` to install the dependencies
